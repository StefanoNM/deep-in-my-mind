﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTab : MonoBehaviour {
	static public MainTab main;
	void Awake () {
		main = GetComponent<MainTab> ();
	}

	public void SpawnPlayer() {
		GameObject player = Instantiate (Settings.player);
		player.transform.parent = transform;
		player.transform.position = transform.position + new Vector3 (0, Settings.playerInitialHeight);
	}
}
