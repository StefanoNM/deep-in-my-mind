﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
	static public Menu main;
	static TabManager tabs;
	static float target;

	void Awake() {
		main = GetComponent<Menu> ();
		tabs = transform.GetChild (0).GetComponent<TabManager> ();
	}

	static public void GetIn() {
		main.transform.position = new Vector3 (Stage.x, -Stage.height);
		target = Stage.y;
		main.enabled = true;
		tabs.enabled = true;
	}
	static public void GetOut() {
		target = 2f * Stage.height;
		main.enabled = true;
		tabs.enabled = false;
	}

	void Update() {
		float distance = target - transform.position.y;
		float delta = Settings.actualVelocity * Time.deltaTime;
		if (distance > delta)
			transform.Translate (Vector3.up * delta);
		else {
			transform.position = new Vector3 (Stage.x, target);
			enabled = false;
		}
	}

	void OnDisable() {
		Game.main.MenuStop ();
	}
}
