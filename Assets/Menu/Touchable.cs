﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Touchable : MonoBehaviour {
	public UnityEvent onTouch;
	public void Touch() {
		onTouch.Invoke ();
	}
}
