﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabManager : MonoBehaviour {
	public float horizontalVelocity;
	public LayerMask touchLayer;
	public float minSwipe;
	bool onSwipe;
	int tabs;
	int currentTab;
	bool idle;
	float initialSwipe;
	float swipe;

	void Start () {
		tabs = transform.childCount;
		GetCurrentTab ();
	}
	void OnEnable() {
		if (Input.touchCount == 1)
			initialSwipe = Camera.main.ScreenToWorldPoint (Input.GetTouch(0).position).x;
	}
	void Update () {
		if (Input.touchCount == 1) {
			Touch touch = Input.GetTouch (0);
			if (touch.phase == TouchPhase.Began) {
				initialSwipe = Camera.main.ScreenToWorldPoint (touch.position).x;
			} else if (touch.phase == TouchPhase.Ended) {
				if (Mathf.Abs (swipe) > minSwipe) {
					GetCurrentTab ();
					idle = false;
				} else {
					Tap (touch.position);
				}
			} else {
				swipe = Camera.main.ScreenToWorldPoint (touch.position).x - initialSwipe;
				transform.position = new Vector3 (-(float)currentTab * Stage.width + swipe, transform.position.y);
			}
		} else if (idle == false)
			MoveToTab ();
		#if UNITY_EDITOR
		if (Game.main.debug) {
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				currentTab++;
				idle = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				currentTab--;
				idle = false;
			}
			if (Input.GetMouseButtonDown (0))
				Tap (Input.mousePosition);
		}
		#endif
	}
	void Tap(Vector2 position) {
		Ray ray = Camera.main.ScreenPointToRay (position);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 20f, (int)touchLayer))
			hit.transform.GetComponent<Touchable> ().Touch ();
	}
	void GetCurrentTab() {
		currentTab = (int)Mathf.Clamp (Mathf.Round (-transform.position.x / Stage.width), 0, tabs - 1);
	}
	void MoveToTab() {
		float target = -(float)currentTab * Stage.width;
		float distance = target - transform.position.x;
		float delta = horizontalVelocity * Time.deltaTime;
		if (Mathf.Abs (distance) > delta) {
			transform.Translate (Vector3.right * delta * Mathf.Sign (distance));
		} else {
			transform.position = new Vector3 (target, transform.position.y);
			idle = true;
		}
	}
}
