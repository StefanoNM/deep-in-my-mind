﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemList : MonoBehaviour {
	public string name;
	public StoreItem[] items;
	public bool bought {
		get {
			if (_bought)
				return true;
			foreach (StoreItem item in items)
				if (!item.bought)
					return false;
			_bought = true;
			return true;
		}
	}
	bool _bought;
	public int currentItem {
		get {
			return PlayerPrefs.GetInt (name + "CurrentItem");
		}
		set {
			PlayerPrefs.SetInt (name + "CurrentItem", value);
		}
	}

	public void Init() {
		foreach (StoreItem item in items)
			item.name = name + item.name;
	}

	public bool Buy(int item) {
		return items [item].Buy ();
	}
}
