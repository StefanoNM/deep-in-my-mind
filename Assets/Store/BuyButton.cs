﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class BuyButton : MonoBehaviour {
	public TextMesh text;
	Renderer rn;

	void Awake() {
		rn = GetComponent<Renderer> ();
	}
	public void ShowPrize(int prize) {
		if (prize == 0)
			ShowFree ();
		else {
			text.text = "$ " + prize;
			rn.material.color = Color.yellow;
		}
	}
	public void ShowFree() {
		text.text = "";
		rn.material.color = Color.green;
	}
	public void Delete() {
		rn.enabled = false;
		GetComponent<Collider> ().enabled = false;
	}
}
