﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorItem : StoreItem {
	public Color color;
}

public class PlayerColor : ItemList {
	public BuyButton buyButton;
	public ColorItem[] colors;
	Renderer rn;

	void Start() {
		rn = GetComponent<Renderer> ();
		items = colors;
		Init ();
		currentItem--;
		Touch ();
		Settings.terrainCubeColor = colors [0].color;
	}

	public void Touch() {
		currentItem++;
		if (currentItem == colors.Length)
			currentItem = 0;
		rn.material.color = colors [currentItem].color;
		if (!bought) {
			if (!items [currentItem].bought)
				buyButton.ShowPrize (items [currentItem].prize);
			else {
				buyButton.ShowFree ();
				Settings.playerColor = colors [currentItem].color;
			}
		}
	}
	public void Bought() {
		items [currentItem].bought = true;
		Settings.playerColor = colors [currentItem].color;
		if (bought)
			buyButton.Delete ();
		else
			buyButton.ShowFree ();
	}
}
