﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreItem : System.Object {
	public string name;
	public int prize;
	public bool bought {
		get {
			if (prize == 0)
				return true;
			return PlayerPrefs.GetInt (name) == 1 ? true : false;
		}
		set {
			PlayerPrefs.SetInt (name, value ? 1 : 0);
		}
	}
	public bool Buy() {
		if (bought)
			return true;
		else if (Money.money >= prize) {
			Money.money -= prize;
			bought = true;
			return true;
		} else
			return false;
	}
}