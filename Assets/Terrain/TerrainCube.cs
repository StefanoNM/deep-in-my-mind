﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class TerrainCube : MonoBehaviour {
	static public float decellerationMultiplier;
	public float setDecellerationMultiplier;
	public float height { get { return GetComponent<Renderer> ().bounds.size.y; } }
	public float width { get { return GetComponent<Renderer> ().bounds.size.x; } }
	float maxHeight;
	Renderer rn;

	void Awake() {
		rn = GetComponent<Renderer> ();
		rn.material.color = Settings.terrainCubeColor;
		decellerationMultiplier = setDecellerationMultiplier;
		maxHeight = Stage.top + height / 2f;
	}
	void Update () {
		transform.Translate (Vector3.up * Settings.actualVelocity * Time.deltaTime);
		if (transform.position.y > maxHeight)
			Destroy (gameObject);
	}

	void OnCollisionEnter2D(Collision2D other) {
		rn.material.color = Settings.playerColor;
		Settings.actualVelocity += Settings.acceleration;
	}
	void OnDestroy() {
		if (rn.material.color != Settings.playerColor) {
			Settings.actualVelocity -= Settings.acceleration * decellerationMultiplier;
			if (Settings.actualVelocity < 0)
				Settings.actualVelocity = 0;
		}
	}
}
