﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour {
	public GameObject terrainCube;
	float terrainHeight;
	float terrainWidth;
	float terrainDistance;
	float countdown;
	float spawnHeight;

	void Start() {
		terrainHeight = terrainCube.GetComponent<TerrainCube> ().height;
		terrainWidth = terrainCube.GetComponent<TerrainCube> ().width;
		terrainDistance = Settings.terrainDistance + terrainHeight;
		countdown = terrainDistance - (FirstTerrain.height - Stage.bottom);
		spawnHeight = Stage.bottom - terrainHeight / 2f;
	}
	void Update () {
		if (countdown < 0) {
			SpawnSingleCube ();
			countdown += terrainDistance;
		}
		countdown -= Time.deltaTime * Settings.actualVelocity;
	}

	void SpawnSingleCube() {
		int pos = Random.Range (0, 5);
		float x = Stage.left + ((float)pos + 0.5f) * terrainWidth;
		Instantiate (terrainCube, new Vector3 (x, spawnHeight), Quaternion.identity);
	}
}
