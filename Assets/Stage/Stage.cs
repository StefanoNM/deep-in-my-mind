﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class Stage : MonoBehaviour {
	public float setHeight;
	public float setWidth;

	static public float height;
	static public float width;
	static public float x;
	static public float y;
	static public float top;
	static public float right;
	static public float bottom;
	static public float left;

	void Awake () {
		height = setHeight;
		width = setWidth;
		Camera cam = GetComponent<Camera> ();
		cam.orthographicSize = height / 2f;
		cam.aspect = width / height;
		x = transform.position.y;
		y = transform.position.y;
		top = y + (height / 2f);
		right = x + (width / 2f);
		bottom = y - (height / 2f);
		left = x - (width / 2f);
	}
}
