﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {
	static public float menuVelocity;
	static public float verticalVelocity;
	static public float actualVelocity;
	static public float acceleration;
	static public GameObject player;
	static public float playerVelocity;
	static public float terrainDistance;
	static public float playerInitialHeight;
	static public Color playerColor;
	static public Color terrainCubeColor;

	public float setMenuVelocity;
	public float setVerticalVelocity;
	public float setAcceleration;
	public GameObject setPlayer;
	public float setPlayerVelocity;
	public float setTerrainDistance;
	public float setPlayerInitialHeight;
	void Awake() {
		menuVelocity = setMenuVelocity;
		verticalVelocity = setVerticalVelocity;
		acceleration = setAcceleration;
		player = setPlayer;
		playerVelocity = setPlayerVelocity;
		terrainDistance = setTerrainDistance;
		playerInitialHeight = setPlayerInitialHeight;
		playerColor = Color.white;
	}
	void Update() {
		Debug.Log (actualVelocity);
	}
}
