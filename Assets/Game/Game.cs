﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {
	public GameObject terrain;
	public GameObject player;
	public bool debug;
	GameObject actualTerrain;
	static public Game main;

	void Start() {
		main = GetComponent<Game> ();
		Settings.actualVelocity = Settings.menuVelocity;
		Menu.GetIn ();
	}

	public void StartGame() {
		Settings.actualVelocity = Mathf.Max (Settings.actualVelocity, Settings.verticalVelocity);
		Menu.GetOut ();
		Player.main.enabled = true;
		actualTerrain = Instantiate (terrain) as GameObject;
		Score.StartLevel ();
	}
	public void EndGame() {
		Destroy (actualTerrain);
		actualTerrain = null;
		Destroy (Player.main.gameObject);
		Settings.actualVelocity = Mathf.Max (Settings.actualVelocity, Settings.menuVelocity);
		Menu.GetIn ();
		MainTab.main.SpawnPlayer ();
		Score.EndLevel ();
	}
	public void MenuStop() {
		if (actualTerrain == null)
			Settings.actualVelocity = 0;
	}
}
