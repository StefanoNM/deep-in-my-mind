﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Renderer))]
public class Player : MonoBehaviour {
	static public Player main;
	static public float height;
	Rigidbody2D rb;
	Renderer rn;
	bool visible;

	void Awake () {
		main = GetComponent<Player> ();
		rb = GetComponent<Rigidbody2D> ();
		rn = GetComponent<Renderer> ();
		enabled = false;
	}

	void Update () {
		height = transform.position.y;
		float v1 = 0;
		if (visible) {
			if (Input.touchCount > 0) {
				foreach (Touch touch in Input.touches) {
					if (touch.position.x > Screen.width / 2f)
						v1++;
					else
						v1--;
				}
			}
			#if UNITY_EDITOR
			if (Game.main.debug) {
				v1 += Input.GetAxisRaw ("Horizontal");
			}
			#endif
		}
		v1 *= Settings.playerVelocity;
		rb.velocity = new Vector3 (v1, rb.velocity.y);
	}

	void OnEnable() {
		rb.gravityScale = 1;
		transform.parent = null;
	}
	void OnDisable() {
		rb.gravityScale = 0;
	}

	void OnBecameVisible() {
		visible = true;
		rn.material.color = Settings.playerColor;
	}
	void OnBecameInvisible() {
		if (visible && enabled)
			Game.main.EndGame ();
	}
}
