﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class Score : MonoBehaviour {
	TextMesh text;
	string id;
	float oldPlayerHeight;
	static float score;

	void Awake() {
		text = GetComponent<TextMesh> ();
		id = text.text;
	}

	static public void StartLevel() {
		score = 0;
	}
	void Update() {
		if (Player.height < oldPlayerHeight) {
			score += Settings.actualVelocity / (oldPlayerHeight - Player.height) / 10000f;
			text.text = id + (int)score;
		}
		oldPlayerHeight = Player.height;
	}
	static public void EndLevel() {
		OldScore.SetScore ((int)score);
		Top.main.EvaluateScore ((int)score);
		Money.main.AddMoney ((int)score);
	}
}
