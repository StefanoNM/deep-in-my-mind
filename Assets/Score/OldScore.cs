﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class OldScore : MonoBehaviour {
	static TextMesh text;
	static string id;

	void Awake() {
		text = GetComponent<TextMesh> ();
		id = text.text;
		text.text = "";
	}

	static public void SetScore(int score) {
		text.text = id + score;
	}
}
