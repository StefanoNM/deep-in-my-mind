﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class Top : MonoBehaviour {
	static public Top main;
	TextMesh text;
	string id;
	int top;

	void Awake() {
		main = GetComponent<Top> ();
		text = GetComponent<TextMesh> ();
		id = text.text;
		top = PlayerPrefs.GetInt ("TopScore");
		if (top > 0)
			text.text = id + top;
		else
			text.text = "";
	}

	public void EvaluateScore(int score) {
		top = Mathf.Max (top, score);
		PlayerPrefs.SetInt ("TopScore", top);
		if (top > 0)
			text.text = id + top;
		else
			text.text = "";
	}
}
