﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class Money : MonoBehaviour {
	static public Money main;
	TextMesh text;
	string id;
	static public int money;

	void Awake() {
		main = GetComponent<Money> ();
		text = GetComponent<TextMesh> ();
		id = text.text;
		money = PlayerPrefs.GetInt ("Money");
		text.text = id + money;
	}

	public void AddMoney(int points) {
		money += points;
		text.text = id + money;
		PlayerPrefs.SetInt ("Money", money);
	}
}
